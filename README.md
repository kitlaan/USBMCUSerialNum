# USB MCU SerialNum

Expose the flash serial number on an Atmel AVR USB MCU, as described in AVR922.

The library is based on PluggableUSB, so is only compatible with IDE 1.6.6 and
higher.

# Notes

This exposes no new USB endpoints.

Have only tested on atmega32u4.