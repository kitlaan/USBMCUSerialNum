// Copyright (c) 2017 Ted M Lin
// All rights reserved.
//
// This software may be modified and distributed under the terms
// of the BSD license.  See the LICENSE file for details.

#include "USBMCUSerialNum.h"

#include <string.h>

#include <avr/boot.h>

static void getFlashSerial(char *output, size_t len)
{
    const size_t SIG_START = 0x0E;
    const size_t SIG_LEN = 10;

    memset(output, 0, len);

    size_t ix = SIG_START;
    while (len > 0 && ix < (SIG_START + SIG_LEN)) {
        const uint8_t result = boot_signature_byte_get(ix);

        if (len) {
            const uint8_t digit = (result >> 4) & 0xF;
            *output = (digit < 10 ? '0' : 'A'-10) + digit;
            len--;
            output++;
        }
        if (len) {
            const uint8_t digit = result & 0xF;
            *output = (digit < 10 ? '0' : 'A'-10) + digit;
            len--;
            output++;
        }

        ix++;
    }
}

#ifdef USBMCUSERIALNUM_USE_SINGLETON
USBSerialNum_ USBSerialNum;
#endif

USBSerialNum_::USBSerialNum_(const char* sn)
    : PluggableUSBModule(0, 0, 0)
{
    // we expose no extra interfaces or descriptor, we're just here to
    // expose a serial number.
    if (sn == NULL) {
        getFlashSerial(serialNum, sizeof(serialNum));
    }
    else {
        memset(serialNum, 0, sizeof(serialNum));
        strlcpy(serialNum, sn, sizeof(serialNum));
    }

    PluggableUSB().plug(this);
}

const char* USBSerialNum_::getSerialNum() const
{
    return serialNum;
}

uint8_t USBSerialNum_::getShortName(char* name)
{
    // hmm, no length boundary...
    memcpy(name, serialNum, SERIALNUM_LEN);
    return SERIALNUM_LEN;
}

bool USBSerialNum_::setup(USBSetup& setup)
{
    return false;
}

int USBSerialNum_::getDescriptor(USBSetup &setup)
{
    return 0;
}

int USBSerialNum_::getInterface(uint8_t *interfaceCount)
{
    return 0;
}
