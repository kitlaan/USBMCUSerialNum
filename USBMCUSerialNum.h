// Copyright (c) 2017 Ted M Lin
// All rights reserved.
//
// This software may be modified and distributed under the terms
// of the BSD license.  See the LICENSE file for details.

#ifndef USBMCUSERIALNUM_H
#define USBMCUSERIALNUM_H

#include <stdint.h>
#include <Arduino.h>

#if ARDUINO < 10606
#error USBMCUSerialNum requires Arduino 1.6.6 or greater
#endif

#if !defined(USBCON)
#error USBMCUSerialNum requires a USB MCU
#endif

#include "PluggableUSB.h"

class USBSerialNum_ : public PluggableUSBModule
{
public:
    USBSerialNum_(const char* sn = NULL);

    const char* getSerialNum() const;
    static const size_t SERIALNUM_LEN = 20;

private:
    char serialNum[SERIALNUM_LEN + 1];

protected:
    bool setup(USBSetup& setup);
    int getDescriptor(USBSetup& setup);
    int getInterface(uint8_t* interfaceCount);
    uint8_t getShortName(char* name);
};

#ifdef USBMCUSERIALNUM_USE_SINGLETON
extern USBSerialNum_ USBSerialNum;
#else
typedef USBSerialNum_ USBSerialNum;
#endif

#endif // USBSERIALNUM_H
