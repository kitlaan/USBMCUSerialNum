#include <Arduino.h>

#define USBMCUSERIALNUM_USE_SINGLETON
#include "USBMCUSerialNum.h"

void setup()
{
    Serial.begin(115200);
    while (!Serial); // needed for leonardo

    delay(200);

    // Note: you have to USE the singleton variable, otherwise it never
    // gets linked into the program!
    Serial.println("Signature: ");
    Serial.println(USBSerialNum.getSerialNum());
}

void loop()
{
    // Do nothing!
    delay(100);
}